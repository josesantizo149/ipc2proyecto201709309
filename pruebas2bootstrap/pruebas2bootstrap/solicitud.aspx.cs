﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Collections;
using MySql.Data.MySqlClient;
using pruebas2bootstrap.codigo;

namespace pruebas2bootstrap
{
    public partial class solicitud : System.Web.UI.Page
    {



        protected void Page_Load(object sender, EventArgs e) 
        {
            if (DropDownList1.Items.Count == 0)
            {
                DropDownList1.Items.Add("Café");
                DropDownList1.Items.Add("Comida Típica");
                DropDownList1.Items.Add("Steak House");
                DropDownList1.Items.Add("Antojitos");
                DropDownList1.Items.Add("Máriscos");
                DropDownList1.Items.Add("Comida China");
                DropDownList1.Items.Add("Pizza");
                DropDownList1.Items.Add("Hamburguesas");
                DropDownList1.Items.Add("Tacos");
                DropDownList1.Items.Add("Pollo frito");
            }

            if (DropDownList4.Items.Count == 0)
            {
                DropDownList4.Items.Add("Norte");
                DropDownList4.Items.Add("Sur");
                DropDownList4.Items.Add("Este");
                DropDownList4.Items.Add("Oeste"); 
                
            }
            if (DropDownList3.Items.Count == 0)
            {
                DropDownList3.Items.Add("Norte");
                DropDownList3.Items.Add("Sur");
                DropDownList3.Items.Add("Este");
                DropDownList3.Items.Add("Oeste");

            }

            if (DropDownList2.Items.Count == 0)
            {
                DropDownList2.Items.Add("Norte");
                DropDownList2.Items.Add("Sur");
                DropDownList2.Items.Add("Este");
                DropDownList2.Items.Add("Oeste");

            }

            if (CheckBoxList1.Items.Count==0)
            {
                CheckBoxList1.Items.Add("Cable");
                CheckBoxList1.Items.Add("Agua caliente");
                CheckBoxList1.Items.Add("Wifi");
                CheckBoxList1.Items.Add("Sauna");
                CheckBoxList1.Items.Add("Gym");
                
            }
        }

        

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text!="")
            {
                string nombre = TextBox1.Text;
                string direccion = TextBox2.Text;
                string telefono = TextBox3.Text;
                string email = TextBox4.Text;
                string horario = TextBox5.Text;
                string region = DropDownList4.SelectedValue;  
                string rol = "Restaurante"; 
                string estado = "Pre-Evaluacion";
                string especialidad = DropDownList1.SelectedValue;

                ingresarRestaurante(nombre,direccion,telefono,email,horario,region,rol,estado,especialidad); 
            }
            else if (TextBox6.Text!="")
            {
                string nombre = TextBox6.Text;
                string direccion = TextBox7.Text;
                string telefono = TextBox8.Text;
                string email = TextBox9.Text;
                string horario = TextBox10.Text;
                string region = DropDownList3.SelectedValue;
                string rol = "Museo";
                string estado = "Pre-Evaluacion";
                string tarifa = TextBox11.Text;

                ingresarMuseo(nombre, direccion, telefono, email, horario, region, rol, estado, tarifa);
            }
            else if (TextBox12.Text != "")
            {
                string nombre = TextBox12.Text;
                string direccion = TextBox13.Text;
                string telefono = TextBox14.Text;
                string email = TextBox15.Text;
                string region = DropDownList2.SelectedValue;
                string rol = "Hotel";
                string estado = "Pre-Evaluacion";

                ingresarHotel(nombre, direccion, telefono, email, region, rol, estado);

                for (int i = 0; i < CheckBoxList1.Items.Count; i++)
                {
                    if (CheckBoxList1.Items[i].Selected)
                    {
                        ingresarServicio(nombre,CheckBoxList1.Items[i].Text); 
                    }
                }
                CheckBoxList1.ClearSelection(); 
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Campos vacios');</script>");
            }

            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox4.Text = "";
            TextBox5.Text = "";
            TextBox6.Text = "";
            TextBox7.Text = "";
            TextBox8.Text = "";  
            TextBox9.Text = "";
            TextBox10.Text = "";
            TextBox11.Text = "";
            TextBox12.Text = "";
            TextBox13.Text = "";
            TextBox14.Text = "";
            TextBox15.Text = "";
        }

        //-------------------------INGRESAR HOTEL----------------------------------------------------------------------------

        public void ingresarServicio(string empresa,string servicio)
        {  

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO serviciohotel VALUES(@a,@b);";

                comando.Parameters.AddWithValue("@a", empresa);
                comando.Parameters.AddWithValue("@b", servicio);
                

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }
        }

        //-------------------------INGRESAR HOTEL----------------------------------------------------------------------------

        public void ingresarHotel(string nombre, string direccion, string telefono, string email, string region,
        string rol, string estado)
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO empresa VALUES(@a,@b,@c,@d,null,@f,@g,@h,null,null);";

                comando.Parameters.AddWithValue("@a", nombre);
                comando.Parameters.AddWithValue("@b", direccion);
                comando.Parameters.AddWithValue("@c", telefono);
                comando.Parameters.AddWithValue("@d", email);
                comando.Parameters.AddWithValue("@f", region);
                comando.Parameters.AddWithValue("@g", rol);
                comando.Parameters.AddWithValue("@h", estado);
                


                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }
        }

        //-------------------------INGRESAR MUSEO----------------------------------------------------------------------------
        public void ingresarMuseo(string nombre, string direccion, string telefono, string email, string horario, string region,
            string rol, string estado, string tarifa) 
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO empresa VALUES(@a,@b,@c,@d,@e,@f,@g,@h,null,@i);";

                comando.Parameters.AddWithValue("@a", nombre);
                comando.Parameters.AddWithValue("@b", direccion);
                comando.Parameters.AddWithValue("@c", telefono);
                comando.Parameters.AddWithValue("@d", email);
                comando.Parameters.AddWithValue("@e", horario);
                comando.Parameters.AddWithValue("@f", region);
                comando.Parameters.AddWithValue("@g", rol);
                comando.Parameters.AddWithValue("@h", estado);
                comando.Parameters.AddWithValue("@i", tarifa); 


                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }
        }

        //---------------------INGRESAR RESTAURANTE------------------------------------------------------------------------------

        public void ingresarRestaurante(string nombre,string direccion, string telefono, string email,string horario,string region,
            string rol,string estado,string especialidad) 
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO empresa VALUES(@a,@b,@c,@d,@e,@f,@g,@h,@i,null);";

                comando.Parameters.AddWithValue("@a", nombre);
                comando.Parameters.AddWithValue("@b", direccion);
                comando.Parameters.AddWithValue("@c", telefono);
                comando.Parameters.AddWithValue("@d", email);
                comando.Parameters.AddWithValue("@e", horario);
                comando.Parameters.AddWithValue("@f", region);
                comando.Parameters.AddWithValue("@g", rol);
                comando.Parameters.AddWithValue("@h", estado);
                comando.Parameters.AddWithValue("@i", especialidad);


                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }
        }

     //---------------------------------------------------------------------------------------------------------------------------

        protected void Button1_Click1(object sender, EventArgs e)
        {

        }
    }
}