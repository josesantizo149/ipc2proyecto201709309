﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using pruebas2bootstrap.codigo;
using MySql.Data.MySqlClient;


namespace pruebas2bootstrap
{
    public partial class administrador : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        { 
            if (DropDownList1.Items.Count==0)
            {
                DropDownList1.Items.Add("Agente");
                DropDownList1.Items.Add("Técnico");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text!=""&& TextBox2.Text != "" && TextBox3.Text != "" && TextBox4.Text != "" && TextBox5.Text != "" && TextBox6.Text  != ""
                 )
            {
                string dpi = TextBox1.Text;
                string nombre = TextBox2.Text;
                string email = TextBox3.Text;
                string telefono = TextBox4.Text;
                string usuario = TextBox5.Text;
                string password = TextBox6.Text;
                string rol = DropDownList1.SelectedItem.Value;
                usuarios.insertarUsuario(dpi,nombre,email,telefono,usuario, password,rol);   
                TextBox1.Text="";
                TextBox2.Text = "";
                TextBox3.Text = "";
                TextBox4.Text = "";
                TextBox5.Text = "";
                TextBox6.Text = "";
                Response.Redirect("administrador.aspx"); 
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(),"Scripts","<script>alert('Campos vacios');</script>"); 
            }
              


        }

  

        protected void Button2_Click1(object sender, EventArgs e)
        {
            if (TextBox1.Text != "" && TextBox2.Text != "" && TextBox3.Text != "" && TextBox4.Text != "" && TextBox5.Text != "" && TextBox6.Text != ""
                )
            {
                string dpi = TextBox1.Text;
                string nombre = TextBox2.Text;
                string email = TextBox3.Text;
                string telefono = TextBox4.Text;
                string usuario = TextBox5.Text;
                string password = TextBox6.Text;
                string rol = DropDownList1.SelectedItem.Value;
                usuarios.editarUsuario(dpi, nombre, email, telefono, usuario, password,rol); 
                TextBox1.Text = "";
                TextBox2.Text = "";
                TextBox3.Text = "";
                TextBox4.Text = "";
                TextBox5.Text = "";
                TextBox6.Text = "";
                Response.Redirect("administrador.aspx");
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Campos vacios');</script>");
            }
        }

        protected void GridView1_SelectedIndexChanged1(object sender, EventArgs e)
        {
            TextBox1.Text = GridView1.SelectedRow.Cells[1].Text;
            TextBox2.Text = GridView1.SelectedRow.Cells[2].Text;
            TextBox3.Text = GridView1.SelectedRow.Cells[3].Text;
            TextBox4.Text = GridView1.SelectedRow.Cells[4].Text;
            TextBox5.Text = GridView1.SelectedRow.Cells[5].Text;
            TextBox6.Text = GridView1.SelectedRow.Cells[6].Text;

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text != "")
            {
                string dpi = TextBox1.Text;
              
                usuarios.eliminarUsuario(dpi);
                TextBox1.Text = "";
                TextBox2.Text = "";
                TextBox3.Text = "";
                TextBox4.Text = "";
                TextBox5.Text = "";
                TextBox6.Text = "";
                Response.Redirect("administrador.aspx");
            } 
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Campo Dpi vacio');</script>");
            }
        }
    }
}