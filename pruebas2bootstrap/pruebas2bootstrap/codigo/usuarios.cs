﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace pruebas2bootstrap.codigo
{
    public class usuarios
    {

        public static void insertarUsuario(string dpi, string nombre,string email, string telefono, string usuario, string password,string rol)
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO usuario VALUES(@a,@b,@c,@d,@e,@f,@g);"; 

                comando.Parameters.AddWithValue("@a", dpi);
                comando.Parameters.AddWithValue("@b", nombre);
                comando.Parameters.AddWithValue("@c", email);
                comando.Parameters.AddWithValue("@d", telefono);
                comando.Parameters.AddWithValue("@e", usuario);
                comando.Parameters.AddWithValue("@f", password);
                comando.Parameters.AddWithValue("@g", rol); 


                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }


        }


        public static void editarUsuario(string dpi, string nombre, string email, string telefono, string usuario, string password,string rol)
        {

            MySqlConnection conectar = conexion.RecibirConexion(); 

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "UPDATE usuario SET dpi =@a,nombre =@b,email =@c,telefono =@d,usuario =@e,password =@f,rol=@g WHERE  dpi =@a;";

                comando.Parameters.AddWithValue("@a", dpi);
                comando.Parameters.AddWithValue("@b", nombre);
                comando.Parameters.AddWithValue("@c", email);
                comando.Parameters.AddWithValue("@d", telefono);
                comando.Parameters.AddWithValue("@e", usuario);
                comando.Parameters.AddWithValue("@f", password);
                comando.Parameters.AddWithValue("@g", rol);


                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }


        }


        public static void eliminarUsuario(string dpi)
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "DELETE FROM usuario WHERE  dpi=@a;"; 

                comando.Parameters.AddWithValue("@a", dpi);
                
                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }

        }


    }
}