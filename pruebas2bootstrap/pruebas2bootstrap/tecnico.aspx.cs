﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using pruebas2bootstrap.codigo;

namespace pruebas2bootstrap
{
    public partial class tecnico : System.Web.UI.Page
    {

         static string dpi = ""; 

        public static void setDpi(string dpi2)
        {
          dpi = dpi2;  
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            empresasEvaluar();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            servicios(DropDownList1.SelectedValue);
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            servicios(DropDownList1.SelectedValue);   
        }

        //APROBAR EMPRESA--------------------------------------------------------------------

            public void aprobada(String empresa)
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "UPDATE empresa SET estado =@b WHERE  nombre =@a;";

                comando.Parameters.AddWithValue("@a", empresa);
                comando.Parameters.AddWithValue("@b", "Aprobada"); 


                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }
        }

        //MOSTRAR SERVICIOS DE HOTEL----------------------------------------------------------

            public void servicios(string empresa) 
        {
            CheckBoxList1.Items.Clear();

            MySqlConnection coneccion = conexion.RecibirConexion();
            MySqlCommand comando = new MySqlCommand("SELECT *  FROM serviciohotel WHERE empresa =@a ;", coneccion);

            comando.Parameters.AddWithValue("@a",empresa); 

            coneccion.Open();
            MySqlDataReader dato = comando.ExecuteReader();
            while (dato.Read())
            {

                CheckBoxList1.Items.Add(dato["servicio"].ToString()); 
                   
            }
            coneccion.Close();

        }

        //INGRESAR EMPRESAS A DROPDOWNLIST-------------------------------------------------------------------
        public void empresasEvaluar() 
        {
            DropDownList1.Items.Clear();
            MySqlConnection coneccion = conexion.RecibirConexion();
            MySqlCommand comando = new MySqlCommand("SELECT *  FROM inspeccion WHERE estado = 'Evaluacion' ; ", coneccion);

            coneccion.Open();
            MySqlDataReader dato = comando.ExecuteReader();
            while (dato.Read())
            {
                if (DropDownList1.Items.FindByText(dato["empresa"].ToString()) == null && dpi == dato["tecnico"].ToString())  
                {
                    DropDownList1.Items.Add(dato["empresa"].ToString());  
                }

            }
            coneccion.Close();

        }

        //APROBAR EN INSPECCION-------------------------------------------------------------------
        public void aprobar(String empresa) 
        {
            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "UPDATE inspeccion SET estado =@b WHERE  empresa =@a;";

                comando.Parameters.AddWithValue("@a", empresa);
                comando.Parameters.AddWithValue("@b", "Aprobada");


                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }

        }

        //----------------------------------------------------------------------------------------------------
        protected void Button2_Click(object sender, EventArgs e)
        {
            aprobada(DropDownList1.SelectedValue);
            aprobar(DropDownList1.SelectedValue);  
        }
        //-------------------------------------------------------------------------------------------------------
    }
}