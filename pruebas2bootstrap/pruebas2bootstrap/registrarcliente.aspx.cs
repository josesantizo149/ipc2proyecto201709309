﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using pruebas2bootstrap.codigo;

namespace pruebas2bootstrap
{
    public partial class registrarcliente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            if (TextBox1.Text!="" && TextBox2.Text!="")
            {
                if (existe(TextBox1.Text)==false)
                {
                    ingresarCliente(TextBox1.Text,TextBox2.Text);
                }
                else
                {
                    Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Usuario ya existe');</script>");  
                }
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Campos vacios');</script>");
            }

        }

        //EXISTE EL CLIENTE-----------------------------------------------------

            public bool existe(string usuario)
        {
            bool respuesta = false;

            MySqlConnection coneccion = conexion.RecibirConexion();
            MySqlCommand comando = new MySqlCommand("SELECT *  FROM cliente ;", coneccion);

            

            coneccion.Open();
            MySqlDataReader dato = comando.ExecuteReader();
            while (dato.Read())
            {
                if (dato["usuario"].ToString()==usuario)
                {
                    respuesta = true;
                    break;
                }
               
            }
            coneccion.Close();

            return respuesta;
        }

        //REGISTRAR CLIENTE------------------------------------------------------

        public void ingresarCliente(string usuario,string password)
        {
            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO cliente VALUES(@a,@b);";

                comando.Parameters.AddWithValue("@a", usuario);
                comando.Parameters.AddWithValue("@b", password);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }
        }

        //-------------------------------------------------------------------------------------------------


    }
}