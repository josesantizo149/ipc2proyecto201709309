﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using pruebas2bootstrap.codigo;

namespace pruebas2bootstrap
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        string nombre = "";
        string rol = "";
        string dpitecnico = "";
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }  

        

        protected void Button1_Click1(object sender, EventArgs e)
        {

            if (TextBox1.Text == "admin" && TextBox2.Text=="admin")
            {
                Response.Redirect("administrador.aspx");
                TextBox1.Text = "";
                TextBox2.Text = "";
            } 
            else if(ingreso(TextBox1.Text,TextBox2.Text)&&rol=="Agente") 
            {
                Response.Redirect("agente.aspx");
                rol = "";
                nombre = "";
                TextBox1.Text = "";
                TextBox2.Text = "";
            }
            else if (ingreso(TextBox1.Text, TextBox2.Text) && rol == "Técnico") 
            {
                tecnico.setDpi(dpitecnico);
                Response.Redirect("tecnico.aspx");
                
                rol = "";
                nombre = "";
                dpitecnico = "";
                TextBox1.Text = "";
                TextBox2.Text = "";
            }
            else if (existeCliente(TextBox1.Text,TextBox2.Text))
            {
                Response.Redirect("cliente.aspx");  
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Usuario no registrado');</script>"); 
            }
        }

        //INGRESO CLIENTES-------------------------------------------------------------------------------------------------

        public bool existeCliente(string usuario,string password)
        {
            bool respuesta = false;

            MySqlConnection coneccion = conexion.RecibirConexion();
            MySqlCommand comando = new MySqlCommand("SELECT *  FROM cliente ;", coneccion);



            coneccion.Open();
            MySqlDataReader dato = comando.ExecuteReader(); 
            while (dato.Read())
            {
                if (dato["usuario"].ToString() == usuario&& dato["password"].ToString() == password)
                {
                    respuesta = true;
                    break;
                }

            }
            coneccion.Close();

            return respuesta;
        }

        //INGRESO USUARIOS--------------------------------------------------------------------------------------------------
        public bool ingreso(string usuario,string password) 
        {
            bool respuesta = false;
            MySqlConnection coneccion = conexion.RecibirConexion();
            MySqlCommand comando = new MySqlCommand("SELECT *  FROM usuario WHERE usuario =@a AND password =@b ;",coneccion);
            comando.Parameters.AddWithValue("@a", usuario);
            comando.Parameters.AddWithValue("@b", password);
            coneccion.Open();
            MySqlDataReader dato = comando.ExecuteReader(); 
            if (dato.Read())
            {
                nombre = dato["nombre"].ToString();
                rol = dato["rol"].ToString();  
                dpitecnico= dato["dpi"].ToString();
                respuesta = true;
            }  
            coneccion.Close(); 
            return respuesta; 
        }
        //---------------------------------------------------------------------------
    }
}