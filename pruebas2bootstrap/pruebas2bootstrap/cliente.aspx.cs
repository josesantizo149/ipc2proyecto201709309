﻿using MySql.Data.MySqlClient;
using pruebas2bootstrap.codigo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace pruebas2bootstrap
{
    public partial class cliente : System.Web.UI.Page
    {

        System.Data.DataTable table;
        System.Data.DataRow row;

        System.Data.DataTable table2;
        System.Data.DataRow row2; 

        protected void Page_Load(object sender, EventArgs e)
        {
            table = new System.Data.DataTable();
            table.Columns.Add("Nombre", typeof(System.String));
            table.Columns.Add("Dirección", typeof(System.String));
            table.Columns.Add("Teléfono", typeof(System.String));
            table.Columns.Add("E-mail", typeof(System.String));
            table.Columns.Add("Horario", typeof(System.String));
            table.Columns.Add("Region", typeof(System.String));
            table.Columns.Add("Rol", typeof(System.String));
            table.Columns.Add("Especialidad", typeof(System.String));
            table.Columns.Add("Tarífa", typeof(System.String));

            table2 = new System.Data.DataTable();
            table2.Columns.Add("Nombre", typeof(System.String));
            table2.Columns.Add("Descripción", typeof(System.String));
            table2.Columns.Add("Región", typeof(System.String));
            

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            
        }

        //EMPRESAS--------------------------------------------------------------------------

        public void mostrarSitios(string b)  
        {


            if (b == "")
            {
                MySqlConnection coneccion = conexion.RecibirConexion();
                MySqlCommand comando = new MySqlCommand("SELECT *  FROM turistico ;", coneccion);


                coneccion.Open();
                MySqlDataReader dato = comando.ExecuteReader();
                while (dato.Read())
                {

                    row2 = table2.NewRow();
                    row2["Nombre"] = dato["nombre"].ToString();
                    row2["Descripción"] = dato["descripcion"].ToString();
                    row2["Región"] = dato["region"].ToString();
                    

                    table2.Rows.Add(row2);  
                    GridView2.DataSource = table2;
                    GridView2.DataBind();

                }
                coneccion.Close();
            }
            else
            {
                MySqlConnection coneccion = conexion.RecibirConexion();  
                MySqlCommand comando = new MySqlCommand("SELECT *  FROM turistico WHERE nombre = @a OR region = @a ;", coneccion);

                comando.Parameters.AddWithValue("@a", b);

                coneccion.Open();
                MySqlDataReader dato = comando.ExecuteReader();
                while (dato.Read())
                {

                    row2 = table2.NewRow();  
                    row2["Nombre"] = dato["nombre"].ToString();
                    row2["Descripción"] = dato["descripcion"].ToString();
                    row2["Región"] = dato["region"].ToString();


                    table2.Rows.Add(row2);
                    GridView2.DataSource = table2;
                    GridView2.DataBind();

                }
                coneccion.Close();
            }


        }

        //EMPRESAS--------------------------------------------------------------------------

        public void mostrarEmpresas(string b) 
        {
           

            if (b=="") 
            {
                MySqlConnection coneccion = conexion.RecibirConexion();
                MySqlCommand comando = new MySqlCommand("SELECT *  FROM empresa WHERE estado = 'Aprobada' ;", coneccion);


                coneccion.Open();
                MySqlDataReader dato = comando.ExecuteReader();
                while (dato.Read())
                {
                    
                    row = table.NewRow();
                    row["Nombre"] = dato["nombre"].ToString();
                    row["Dirección"] = dato["direccion"].ToString();
                    row["Teléfono"] = dato["telefono"].ToString();
                    row["E-mail"] = dato["email"].ToString();
                    row["Horario"] = dato["horario"].ToString();
                    row["Region"] = dato["region"].ToString();
                    row["Rol"] = dato["rol"].ToString();
                    row["Especialidad"] = dato["especialidad"].ToString();
                    row["Tarífa"] = dato["tarifa"].ToString();

                    table.Rows.Add(row);
                    GridView1.DataSource = table;
                    GridView1.DataBind();
                      
                }
                coneccion.Close();
            }
            else
            {
                MySqlConnection coneccion = conexion.RecibirConexion();
                MySqlCommand comando = new MySqlCommand("SELECT *  FROM empresa WHERE estado = 'Aprobada' AND (nombre = @a OR region = @a) ;", coneccion); 

                comando.Parameters.AddWithValue("@a", b);

                coneccion.Open();
                MySqlDataReader dato = comando.ExecuteReader();
                while (dato.Read())
                {

                    row = table.NewRow();
                    row["Nombre"] = dato["nombre"].ToString();
                    row["Dirección"] = dato["direccion"].ToString();
                    row["Teléfono"] = dato["telefono"].ToString();
                    row["E-mail"] = dato["email"].ToString();
                    row["Horario"] = dato["horario"].ToString();
                    row["Region"] = dato["region"].ToString();
                    row["Rol"] = dato["rol"].ToString();
                    row["Especialidad"] = dato["especialidad"].ToString();
                    row["Tarífa"] = dato["tarifa"].ToString();

                    table.Rows.Add(row);
                    GridView1.DataSource = table;
                    GridView1.DataBind();

                }
                coneccion.Close();
            }
           

        }

        //----------------------------------------------------------------------------------

        protected void Button1_Click(object sender, EventArgs e)
        {

            mostrarEmpresas(TextBox1.Text);
            mostrarSitios(TextBox1.Text);


        }
    }
}