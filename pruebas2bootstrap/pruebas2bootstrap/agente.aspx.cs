﻿using MySql.Data.MySqlClient;
using pruebas2bootstrap.codigo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace pruebas2bootstrap
{
    public partial class agente : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            tecnicos();

            if (DropDownList1.Items.Count == 0)
            {
                DropDownList1.Items.Add("Norte");
                DropDownList1.Items.Add("Sur");
                DropDownList1.Items.Add("Este");
                DropDownList1.Items.Add("Oeste");

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text != "" && TextBox2.Text != "")
            {
                string nombre = TextBox1.Text;
                string descripcion = TextBox2.Text;
                string region = DropDownList1.SelectedValue;
                ingresarSitio(nombre, descripcion, region);
                Response.Redirect("agente.aspx");
                TextBox1.Text = "";
                TextBox2.Text = "";
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Campos vacios');</script>");
            }

        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text != "" && TextBox2.Text != "")
            {
                string nombre = TextBox1.Text;
                string descripcion = TextBox2.Text;
                string region = DropDownList1.SelectedValue;
                editarSitio(nombre, descripcion, region);
                Response.Redirect("agente.aspx");
                TextBox1.Text = "";
                TextBox2.Text = "";
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Campos vacios');</script>");
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            if (TextBox1.Text != "")
            {
                string nombre = TextBox1.Text;

                eliminarSitio(nombre);
                Response.Redirect("agente.aspx");
                TextBox1.Text = "";
                TextBox2.Text = "";
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Scripts", "<script>alert('Campos vacios');</script>");
            }

        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            if (TextBox3.Text!="")
            {
                string empresa = TextBox3.Text;
                recibo(empresa); 
            }
            

        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            if (TextBox3.Text != "")
            {
                string empresa = TextBox3.Text;
                string[] tecnico = DropDownList2.SelectedValue.Split(' ');    
                inspeccion(empresa,tecnico[0]);
                preevaluacion(empresa); 
            }

            TextBox3.Text = ""; 
        }
        //ASIGNAR INSPECCION-------------------------------------------------------------------------------------------------

            public void preevaluacion(string empresa) 
        {
            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "UPDATE empresa SET estado =@b WHERE  nombre =@a;";

                comando.Parameters.AddWithValue("@a", empresa);
                comando.Parameters.AddWithValue("@b", "Evaluacion"); 


                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }
        }

        //ASIGNAR INSPECCION-------------------------------------------------------------------------------------------------
        public void inspeccion(string empresa,string tecnico)
        {
            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO inspeccion VALUES (@a,@b,@c);";

                comando.Parameters.AddWithValue("@a", "Evaluacion");
                comando.Parameters.AddWithValue("@b", empresa);
                comando.Parameters.AddWithValue("@c", tecnico);  


                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }
        }
        //HACER RECIBO-------------------------------------------------------------------------------------------------------

        public void recibo(string empresa)
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO recibo (estado,empresa) VALUES (@a,@b);"; 

                comando.Parameters.AddWithValue("@a", "pendiente");
                comando.Parameters.AddWithValue("@b", empresa); 

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }
        }
            

        //ELIMINAR SITIO-------------------------------------------------------------------------------------------------------
        public  void eliminarSitio(string nombre)
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "DELETE FROM turistico WHERE  nombre=@a;";

                comando.Parameters.AddWithValue("@a", nombre); 

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }

        }
        //EDITAR SITIO----------------------------------------------------------------------------------------------------------
        public  void editarSitio(string nombre, string descripcion, string region)
        {

            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "UPDATE turistico SET nombre =@a,descripcion =@b,region =@c WHERE  nombre =@a;";

                comando.Parameters.AddWithValue("@a", nombre);
                comando.Parameters.AddWithValue("@b", descripcion);
                comando.Parameters.AddWithValue("@c", region);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }


        }
        //INGRESAR SITIO---------------------------------------------------------------------------------------------------------

        public  void ingresarSitio(string nombre,string descripcion,string region)
        {
            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO turistico VALUES(@a,@b,@c);";

                comando.Parameters.AddWithValue("@a", nombre);
                comando.Parameters.AddWithValue("@b", descripcion);
                comando.Parameters.AddWithValue("@c", region);
                
                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception) { }

            }
            catch (Exception) { }
        }

        //--------------TECNICOS DISPONIBLES-------------------------------------------------------------------------------------------

            public void tecnicos()
        {
             

            MySqlConnection coneccion = conexion.RecibirConexion();
            MySqlCommand comando = new MySqlCommand("SELECT *  FROM usuario WHERE rol = 'Técnico' ;", coneccion);
            
            coneccion.Open();
            MySqlDataReader dato = comando.ExecuteReader();
            while (dato.Read())
            {
                if (DropDownList2.Items.FindByText(dato["dpi"].ToString() + " " + dato["nombre"].ToString())==null)
                {
                    DropDownList2.Items.Add(dato["dpi"].ToString() + " " + dato["nombre"].ToString());
                }
               
            }
            coneccion.Close();
        }


        //---------------GRID VIEW SITIOS------------------------------------------------------------------------------------------------
        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBox1.Text = GridView2.SelectedRow.Cells[1].Text;
            TextBox2.Text = GridView2.SelectedRow.Cells[2].Text;
        }

        //---------------GRID VIEW EMPRESAS------------------------------------------------------------------------------------------------
       

        protected void GridView3_SelectedIndexChanged1(object sender, EventArgs e)
        {
            TextBox3.Text = GridView3.SelectedRow.Cells[1].Text;
        }


        //-------------------------------------------------------------------------------------------------------------------
    }
}